package arrays;

// suppose 234 and 368 ans will be 602
public class Program5 {
  public static void sumOfTwoArrays(int arr1[], int arr2[], int output[]) {
    //Your code goes here
      int M = arr1.length - 1;
      int N = arr2.length - 1;
      int K = output.length - 1;
      int carry = 0;
     
      while(M >= 0 || N >= 0){
          int mDigit = M >= 0 ? arr1[M] : 0;
          int nDigit = N >=0 ? arr2[N] : 0;
          int sum = mDigit + nDigit;
          output[K] = (carry + sum) % 10;
          carry = sum / 10;
          K--;
          M--;
          N--;
      }
     
     output[0] = carry;
  }
}
