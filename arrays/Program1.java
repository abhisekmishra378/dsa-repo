package arrays;
// Problem is to sort array of 0 and 1's in 1 iteration.

public class Program1 {
  public static void sortZeroesAndOne(int[] arr) {
      int index = -1;
      for(int i=0; i<arr.length; i++){
          if(arr[i] == 0 && index >= 0)
          {
              // swap(arr, i, index); create a swap function
              index++;
          }
          else if(arr[i] == 1 && index < 0)
          index = i;
      }
  }
}
