package arrays;

// A array of size N given. Inside this array it is guarantee that it contains 0 to N-2 and one elemenet 2 times.
//N = 5 , arr = {0,1,2,3,1};
public class Program3 {
  public static int duplicateNumber(int arr[]) {
    //Your code goes here
      // xor properties are associative in nature
      // A^B = B^A
      //A^0 = A

      int n = arr.length;
      //xor from 1 to n - 2
      int ans = 0;
      for(int i=1; i<=n-2; i++){
          ans ^= i;
      }
      //xor from arr[0] to arr[length]
      for(int i=0; i<n; i++)
      ans ^= arr[i];

      return ans;
  }
}
