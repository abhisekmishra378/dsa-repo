package arrays;

//two sum problem
//calculate the total pairs
//array can contains duplicate

public class Program7 {
  public static int pairSum(int arr[], int x) {
    //Your code goes here
      int count = 0;
      java.util.Arrays.sort(arr);
      //O(nlogn)
      int s = 0;
      int n = arr.length;
      int e = n - 1;
      while(s<e){
          if(arr[s] + arr[e] ==  x){
              int c = 1;
              int d = 1;
              if(s+1 < n && arr[s+1] == arr[s])
              {
                  for(int i=s+1; i<n; i++){
                      if(arr[i] == arr[i-1])
                      c++;
                      else {
                          s = i;
                          break;
                      }
                  }
              }
              else {
                  s++;
              }

              if(e - 1 >= 0 && arr[e - 1] == arr[e]){
                    for(int i=e-1; i>=0; i--){
                     if(arr[i] == arr[i+1])
                     d++;
                     else {
                         e = i;
                         break;
                     }
                  }
              }
              else {
                  e--;
              }
              count = count + (c*d);
          }
          else if(arr[s] + arr[e] < x)
          s++;
          else
          e--;
      }
  return count;
  }
}

/*
 * Triplet sum count with duplicates:
 * change the two sum funcyion to function(arr, sum, start, end)
 * then for each array value use the above approch
 * T.C: N^2  + nlogn 
 */