package arrays;

import java.util.HashMap;

public class Program2 {
    public static void intersections(int arr1[], int arr2[]) {
    	//Your code goes here
        HashMap<Integer, Integer> h = new HashMap<>();
        //frequency for arr2
        for(int i=0; i<arr2.length; i++){
            if(h.containsKey(arr2[i]))
                h.put(arr2[i], (int)h.get(arr2[i]) + 1);
            else 
                h.put(arr2[i], 1);
        }

        int count = 0;
        for(int i=0; i<arr1.length; i++){
             if(h.containsKey(arr1[i])){
            count = (int)h.get(arr1[i]);
            if(count > 0)
            {
                 h.put(arr1[i], count - 1);
                 System.out.print(arr1[i] + " ");
            }
             }
        }
    }
}

//SC: O(n)
// TC: O(nlogn)