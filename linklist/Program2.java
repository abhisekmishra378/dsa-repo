package linklist;
/*
 * Insert, delete operation in linklist
 */
public class Program2 {
  private static Node inertAtHead(Node head, int data) {
    Node temp = new Node(data);
    if(head == null)
    return temp;

    temp.next = head;
    return temp;
  }

  private static Node insertAtTail(Node head, int data) {
    Node temp = new Node(data);
    if(head == null)
    return temp;
    if(head.next == null)
    {
      head.next = temp;
      return head;
    }
    Node mover = head;
    while(mover.next != null) {
      mover = mover.next;
    }
    mover.next = temp;
    return head;
  }

  private static Node insertAtKth(Node head, int data, int k) {
    if(head == null)
    return null;
    if(k == 1)
    return inertAtHead(head, data);

    Node temp = new Node(data);
    int cnt = 1;
    Node mover = head;
    while(cnt != k && mover != null){
      cnt++;
      if(cnt == k)
      break;
      mover = mover.next;
    }
    if(mover != null){
      temp.next = mover.next;
      mover.next = temp;
    }
    
    return head;
  }

  private static void printLinkList(Node head) {
    Node tempNode = head;
    while (tempNode != null) {
      System.out.print(tempNode.data + " ");
      tempNode = tempNode.next;
    }
  }

  public static void main(String[] args) {
    Node head = null;
    head = inertAtHead(head, 10);
    head = inertAtHead(head, 20);
    head = inertAtHead(head, 5);
    head = insertAtTail(head, 22);
    head = insertAtTail(head, 25);

    head = insertAtKth(head, 2, 2);
    head = insertAtKth(head, 12, 6);
    head = insertAtKth(head, 120, 10);
    printLinkList(head);
  }
}
