package linklist;

//Function to find the data of nth node from the end of a linked list.

public class Program5 {
  //approch - 1
  // find length then use simple math (length - n) + 1
   int getNthFromLast(Node head, int n)
   {
     // Your code here	
     // first find length of list
     Node curr = head;
     int count = 0;
     while(curr != null){
         curr = curr.next;
         count++;
     }
     if(n > count)
     return -1;
     
     int upto = (count - n) + 1;
     curr = head;
     
     for(int i=1; i<upto; i++)
       curr = curr.next;
       
       return curr.data;
   }

   int getNthFromLastWithoutCount(Node head, int n){
    //first move the start pointer n position ahead
    // then take last pointer at head
    // move start and last by one
    // when start at null the last at the nth node from end of link list
    if(head == null)
    return -1;

    Node start = head;
    for(int i=0; i<n; i++) {
      if(start == null) return -1;
      start = start.next;
    }

    Node end = head;
    while(start != null){
      start = start.next;
      end = end.next;
    }
    return end.data;
   }
}
