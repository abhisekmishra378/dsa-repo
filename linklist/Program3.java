package linklist;

// Insert in a sorted array

public class Program3 {
  static Node insertInSorted(Node head, int data)
  {
      Node temp = new Node(data);
      //info: in no node exist
      if(head == null)
      return temp;
      //info: check with the first node
      if(head.data > data)
      {
          temp.next = head;
          return temp;
      }
      
      //check while traversing the link list.
      boolean isInsert = false;
      Node curr = head;
      while(curr.next != null){
          if(curr.data < data && curr.next.data < data)
          curr = curr.next;
          else {
              temp.next = curr.next;
              curr.next = temp;
              isInsert = true;
              break;
          }
      }
      if(!isInsert){
          curr.next = temp;
      }
      return head;
  }
}
