package linklist;

/*
 * 1. convert an array into linkedlist
 * 2. length of the linkedlist
 */
public class Program1 {
  private static Node converArrayToLinkedList(int arr[]){
    if(arr.length == 0)
    return null;
    Node head = new Node(arr[0]);
    Node mover = head;
    for(int i=1; i<arr.length; i++) {
      Node temp = new Node(arr[i]);
      mover.next = temp;
      mover = temp;
    }
    return head;
  }
  private static int lengthOfLinkedList(Node head) {
    int count = 0;
    Node temp = head;
    while(temp != null) {
      count++;
      temp = temp.next;
    }
    return count;
  }
  //ese hi search bana lenge
  public static void main(String[] args) {
    int arr[] = {1,2,3,4,5};
    Node head = converArrayToLinkedList(arr);
    System.out.println(head);
    Node mover = head;
    while(mover != null){
      System.out.print(mover.data + " ");
      mover = mover.next;
    }
    System.out.println();
    System.out.println("length of linked list "+ lengthOfLinkedList(head));
  }
}
