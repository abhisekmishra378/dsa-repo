package linklist;
   //The sorting can either be non-increasing or non-decreasing.

public class Program4 {
   public static boolean isSorted(Node head)
   {
       // your code here 
       if(head == null || head.next == null)
       return true;
       
       
       boolean isAsc = true;
       boolean isDec = true;
       
       //check for asc
       Node curr = head;
       while(curr.next != null){
           if(curr.data > curr.next.data)
           {
               isAsc = false;
               break;
           }
           curr = curr.next;
       }
       
       //check for desc
       curr = head;
        while(curr.next != null){
           if(curr.data < curr.next.data)
           {
               isDec = false;
               break;
           }
           curr = curr.next;
       }
       return isAsc || isDec;
   }

}
