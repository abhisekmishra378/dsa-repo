package linklist;

public class Program7 {
  //Function to remove duplicates from sorted linked list.
  public static Node removeDuplicates(Node head)
  {
    // Your code here	
    if(head == null || head.next == null)
    return head;

    Node curr = head;
    while(curr != null && curr.next != null){
      if(curr.data != curr.next.data)
        curr = curr.next;
      else 
        curr.next = curr.next.next;
    }

    return head;
  }
}
