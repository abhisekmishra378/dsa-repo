package linklist;
//Function to reverse a linked list.

public class Program6 {
   Node helper(Node head, Node prev){
    //base case
    if(head == null)
    return prev;
    
    //ek case solve karo
    Node next = head.next;
    head.next = prev;
    prev = head;
    //baki recursion sambhal dega
    return helper(next, prev);
}

  Node reverseList(Node head)
  {
      return helper(head, null);
  }

  Node reverseListPartTwo(Node head) {
    if(head == null || head.next == null)
    return head;

    Node reverseHead = reverseListPartTwo(head.next);
    Node tail = head.next;
    tail.next = head;
    head.next = null;

    return reverseHead;
  }
}
